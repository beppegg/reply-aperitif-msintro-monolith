Microservice demo application: Monolithic version
=================================================

This is a toy application written for demo purposes for the first
Open Reply Technoaperitif, "Introduction to Microservices".

It implements a fake backend for an imaginary TV company, and it's
implemented as a monolith for the sake to show how to carve it into
microservices.

Being a toy app, I cut me some slack with the best engineering approach
and I took some filthy shortcuts in order to quicken development. Please
understand that and don't copy my practices as they are shown here!:-D

Use Cases
---------

![](doc/models/uc-mono2micro.png)
