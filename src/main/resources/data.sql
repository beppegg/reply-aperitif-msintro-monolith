insert into User(userid, name, earned_points) values ('sample1', 'Sam Ple Sr.', 0);
insert into User(userid, name, earned_points) values ('sample2', 'Sam Ple Jr.', 0);

insert into Package(code, name, description, price) values ('cinema', 'cinema', 'the most awesome films!', 9);
insert into Package(code, name, description, price) values ('sport', 'sport', 'the most exciting games!', 15);
insert into Package(code, name, description, price) values ('box-set', 'TV box set', 'the most entertaining series!', 12);

insert into User_Packages(userid, product_code) values ('sample1', 'cinema');
insert into User_Packages(userid, product_code) values ('sample1', 'sport');
insert into User_Packages(userid, product_code) values ('sample2', 'cinema');
insert into User_Packages(userid, product_code) values ('sample2', 'sport');
insert into User_Packages(userid, product_code) values ('sample2', 'box-set');

insert into Prize(id, description, points_needed, available_in_warehouse, version) values (0, 'microwave', 100, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (1, 'blender', 100, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (2, 'tv color', 200, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (3, 'streaming set-top box', 200, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (4, 'beats headset', 200, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (5, 'tablet', 500, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (6, 'gaming console', 800, 300, 0);
insert into Prize(id, description, points_needed, available_in_warehouse, version) values (7, 'last generation phone', 1000, 300, 0);

insert into Poll(id, description) values (0, 'Who will perform at Reply XMas Party?');

insert into Poll_Option(pollid, id, description, votes) values (0, 0, 'Frank Sinatra', 0);
insert into Poll_Option(pollid, id, description, votes) values (0, 1, 'Ligabue', 0);
insert into Poll_Option(pollid, id, description, votes) values (0, 2, 'Bob Sinclair', 0);
