package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.web;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.PrizesRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.UsersRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Prize;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/rewards")
public class FidelityService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PrizesRepository prizesRepository;

    @GetMapping("")
    public Collection<Prize> getPrizeFor(@RequestParam(value = "collectableFrom", required = false) String userId) {
        if (userId == null) {
            return this.prizesRepository.findAll();
        } else {
            final Optional<User> user = this.usersRepository.findByUserid(userId);
            if (user.isPresent()) {
                return this.prizesRepository.findByPointsNeededLessThanEqual(user.get().getEarnedPoints());
            } else {
                throw new IllegalArgumentException("User " + userId + " not found");
            }
        }
    }

    @GetMapping("/{prizeId}")
    public Prize getPrize(@PathVariable long prizeId) {
        return this.prizesRepository.findById(prizeId).orElseThrow(IllegalArgumentException::new);
    }

    @PutMapping("/{prizeId}")
    public void claimPrize(@PathVariable long prizeId,
                           @RequestParam(value = "claimedBy") String userId) {
        final Optional<User> possibleUser = this.usersRepository.findByUserid(userId);
        final Optional<Prize> possiblePrize = this.prizesRepository.findById(prizeId);

        if (possibleUser.isPresent() && possiblePrize.isPresent()) {
            final User user = possibleUser.get();
            final Prize prize = possiblePrize.get();

            if (user.getEarnedPoints() >= prize.getPointsNeeded() && prize.getAvailableInWarehouse() > 0) {
                prize.decrementAvailability();
                user.setEarnedPoints(user.getEarnedPoints() - prize.getPointsNeeded());

                this.prizesRepository.save(prize);
                this.usersRepository.save(user);
            } else {
                throw new IllegalArgumentException("User doesn't have enoough points or no more pieces in warehouse");
            }
        } else {
            throw new IllegalArgumentException("User " + userId + " or prize " + prizeId + " not found");
        }
    }

}