package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.PollOption;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface PollOptionsRepository extends Repository<PollOption, Long> {

    Optional<PollOption> findById(PollOption.PollOptionId id);

    void save(PollOption option);
}
