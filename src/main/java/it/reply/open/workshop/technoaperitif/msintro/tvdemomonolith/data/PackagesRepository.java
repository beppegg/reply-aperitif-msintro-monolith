package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Package;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface PackagesRepository extends Repository<Package, String> {

    Optional<Package> findByCode(String code);
    List<Package> findAll();
}
