package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvStationDemoMonolithicVersionApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvStationDemoMonolithicVersionApplication.class, args);
	}
}
