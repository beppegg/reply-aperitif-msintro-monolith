package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Prize;
import org.springframework.data.repository.Repository;

import java.util.Collection;
import java.util.Optional;

public interface PrizesRepository extends Repository<Prize, Long> {

    Collection<Prize> findAll();

    Optional<Prize> findById(long id);

    Collection<Prize> findByPointsNeededLessThanEqual(int threshold);

    void save(Prize prize);
}
