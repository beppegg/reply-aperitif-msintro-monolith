package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.User;
import org.springframework.data.repository.Repository;

import java.util.Collection;
import java.util.Optional;

public interface UsersRepository extends Repository<User, String> {

    Collection<User> findAll();

    Optional<User> findByUserid(String id);

    void save(User user);
}
