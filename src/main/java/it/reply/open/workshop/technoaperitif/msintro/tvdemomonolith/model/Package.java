package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Package {

    @Id
    @Nonnull
    String code;

    @Column(nullable = false, unique = true)
    @Nonnull
    String name;

    @Column(nullable = false)
    @Nonnull
    String description;

    @Column(nullable = false, precision = 11, scale = 2)
    @Nonnull
    BigDecimal price;

}

