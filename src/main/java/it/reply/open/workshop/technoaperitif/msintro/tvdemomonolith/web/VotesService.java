package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.web;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.PollOptionsRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.PollsRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.UsersRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Poll;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.PollOption;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/polls")
public class VotesService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PollsRepository pollsRepository;

    @Autowired
    private PollOptionsRepository pollOptionsRepository;

    @Value("${tvapp.fedelity.grant-points.forVoting:0}")
    private int fidelityPointsForVoting;

    @GetMapping("")
    public Collection<Poll> getPollsList() {
        return this.pollsRepository.findAll();
    }

    @GetMapping("/{pollNumber}")
    public Poll getPoll(@PathVariable long pollNumber) {
        return this.pollsRepository.findById(pollNumber).orElseThrow(IllegalArgumentException::new);
    }

    @GetMapping("/{pollNumber}/options")
    public List<PollOption> getPollOptions(@PathVariable long pollNumber) {
        return this.pollsRepository.findById(pollNumber)
                                   .map(Poll::getOptions)
                                   .orElseThrow(IllegalArgumentException::new);
    }

    @GetMapping("/{pollNumber}/options/{optionId}")
    public PollOption getPollOptions(@PathVariable long pollNumber, @PathVariable long optionId) {
        return this.pollOptionsRepository.findById(new PollOption.PollOptionId(pollNumber, optionId))
                                         .orElseThrow(IllegalArgumentException::new);
    }

    @PostMapping("/{pollNumber}/options/{optionId}/{userId}")
    public void vote(@PathVariable long pollNumber, @PathVariable long optionId, @PathVariable String userId) {

        final Optional<User> user = this.usersRepository.findByUserid(userId);
        if (user.isPresent()) {
            final Optional<PollOption> pollOption =
                    this.pollOptionsRepository.findById(new PollOption.PollOptionId(pollNumber, optionId));
            if (pollOption.isPresent()) {
                final PollOption existentPollOption = pollOption.get();
                existentPollOption.setVotes(existentPollOption.getVotes() + 1);
                this.pollOptionsRepository.save(existentPollOption);

                final User existingUser = user.get();
                existingUser.setEarnedPoints(existingUser.getEarnedPoints() + fidelityPointsForVoting);
                this.usersRepository.save(existingUser);
            } else {
                throw new IllegalArgumentException("Option " + optionId + " for poll " + pollNumber + " not found");
            }
        } else {
            throw new IllegalArgumentException("User " + userId + " not found.");
        }
    }
}