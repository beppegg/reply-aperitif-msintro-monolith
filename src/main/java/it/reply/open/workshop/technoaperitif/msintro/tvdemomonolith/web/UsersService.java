package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.web;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.PackagesRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.UsersRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Package;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UsersService {

    @Autowired
    private PackagesRepository packagesRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Nonnull
    @GetMapping("")
    public Collection<User> getAllUsers() {
        return this.usersRepository.findAll();
    }

    @GetMapping("/{userId}")
    public User getUser(@NotNull @PathVariable String userId) {
        return this.usersRepository.findByUserid(userId).orElseThrow(IllegalArgumentException::new);
    }

    @Nonnull
    @GetMapping("/{userId}/packages")
    public Collection<Package> getPackagesOfUser(@NotNull @PathVariable String userId) {
        return this.usersRepository.findByUserid(userId).map(User::getPackages).orElse(Collections.emptySet());
    }


    @PostMapping("/{userId}/packages/{packageCode}")
    public void buyPackage(@NotNull @PathVariable String userId,
                           @NotNull @PathVariable String packageCode) {
        final Optional<User> user = this.usersRepository.findByUserid(userId);
        final Optional<Package> desiredPackage = this.packagesRepository.findByCode(packageCode);

        if (user.isPresent() && desiredPackage.isPresent()) {
            final Set<Package> userPackages = user.get().getPackages();
            if (userPackages.stream().map(Package::getCode).anyMatch(packageCode::equals)) {
                throw new IllegalArgumentException("User " + userId + " already owns package " + packageCode);
            } else {
                userPackages.add(desiredPackage.get());
                this.usersRepository.save(user.get());
            }
        } else {
            throw new IllegalArgumentException("User " + userId + " or package " + packageCode + " don't exist");
        }
    }

    @PutMapping("/{userId}")
    public void grantPoints(@NotNull @PathVariable String userId,
                            @NotNull @RequestBody User userData) {
        //noinspection PointlessBooleanExpression
        if ("".equals(userId) || userId.equals(userData.getUserid()) == false) {
            throw new IllegalArgumentException("User id " + userId + " not matching with request body");
        }

        final Optional<User> user = this.usersRepository.findByUserid(userId);
        if (user.isPresent()) {
            final User updated = user.get();
            updated.setEarnedPoints(userData.getEarnedPoints());
            this.usersRepository.save(updated);
        }
    }
}
