package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.web;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.BillingsRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.PackagesRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.UsersRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Bill;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Package;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/billings")
public class BillingService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private BillingsRepository billingsRepository;

    @Value("${tvapp.fedelity.grant-points.forBills:0}")
    private int fidelityPointsForPayedBill;

    @GetMapping("/{billNumber}")
    public Bill getBill(@PathVariable long billNumber) {
        return this.billingsRepository.findByNumber(billNumber).orElseThrow(IllegalArgumentException::new);
    }

    @Nonnull
    @PostMapping("")
    public Collection<Long> generateTodayBillings() {
        final Collection<User> allUsers = this.usersRepository.findAll();

        final List<Long> billsId = new ArrayList<>(allUsers.size());
        for (final User user : allUsers) {
            final Bill bill = this.billingsRepository.save(new Bill(null, LocalDate.now(), user,
                                                                    user.getPackages()
                                                                        .stream()
                                                                        .map(Package::getPrice)
                                                                        .reduce(BigDecimal.ZERO, BigDecimal::add),
                                                                    false
            ));
            billsId.add(bill.getNumber());
        }
        return billsId;
    }

    @PutMapping("/{billNumber}")
    public void updateBill(@PathVariable long billNumber, @RequestBody Bill billData) {
        if (!(null != billData.getNumber() && billData.getNumber().equals(billNumber))){
            throw new IllegalArgumentException("Bill number " + billNumber + " doesn't match body data");
        }

        final Optional<Bill> bill = this.billingsRepository.findByNumber(billNumber);
        if (bill.isPresent()) {
            final Bill payedBill = bill.get();
            final boolean wasAlreadyPayed = payedBill.isPayed();
            payedBill.setPayed(billData.isPayed());
            this.billingsRepository.save(payedBill);

            // if a new bill has been payed, grant the user some points.
            if (!wasAlreadyPayed && payedBill.isPayed()) {
                final Optional<User> user = this.usersRepository.findByUserid(payedBill.getCustomer().getUserid());
                if (user.isPresent()) {
                    final User existingUser = user.get();
                    existingUser.setEarnedPoints(existingUser.getEarnedPoints() + fidelityPointsForPayedBill);
                    this.usersRepository.save(existingUser);
                }
            }
        } else {
            throw new IllegalArgumentException("Bill " + billNumber + " not found.");
        }
    }
}