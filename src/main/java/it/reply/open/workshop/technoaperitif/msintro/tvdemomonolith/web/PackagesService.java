package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.web;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data.PackagesRepository;
import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Package;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.Collection;

@RestController
@RequestMapping("/packages")
public class PackagesService {

    @Autowired
    private PackagesRepository packagesRepository;

    @Nonnull
    @GetMapping("")
    public Collection<Package> getAllAvailablePackages() {
        return this.packagesRepository.findAll();
    }


}
