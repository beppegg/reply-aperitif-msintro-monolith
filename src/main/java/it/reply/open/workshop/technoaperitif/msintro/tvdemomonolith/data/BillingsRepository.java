package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Bill;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface BillingsRepository extends Repository<Bill, Long> {

    Bill save(Bill unsaved);

    Optional<Bill> findByNumber(long billNumber);
}
