package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.data;

import it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model.Poll;
import org.springframework.data.repository.Repository;

import java.util.Collection;
import java.util.Optional;

public interface PollsRepository extends Repository<Poll, Long> {

    Collection<Poll> findAll();

    Optional<Poll> findById(long id);

}
