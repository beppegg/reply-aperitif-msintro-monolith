package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.annotation.Nonnull;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Bill {

    @Id
    @GeneratedValue
    Long number;

    @Column(nullable = false)
    @Nonnull
    LocalDate date;

    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    @Nonnull
    User customer;

    @Column(nullable = false, precision = 11, scale = 2)
    @Nonnull
    BigDecimal amount;

    @Column(nullable = false)
    boolean payed = false;

}

