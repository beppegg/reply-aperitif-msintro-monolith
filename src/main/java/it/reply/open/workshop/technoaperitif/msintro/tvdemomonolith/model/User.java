package it.reply.open.workshop.technoaperitif.msintro.tvdemomonolith.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.annotation.Nonnull;
import javax.persistence.*;
import java.util.Set;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @Nonnull
    String userid;

    @Column(nullable = false)
    @Nonnull
    String name;

    @Column(nullable = false)
    int earnedPoints = 0;

    @ManyToMany
    @JoinTable(name = "User_Packages",
               joinColumns = {@JoinColumn(name = "userid")},
               inverseJoinColumns = {@JoinColumn(name = "product_code")})
    @Nonnull
    @JsonIgnore
    Set<Package> packages;
}
